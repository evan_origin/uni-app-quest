
function writeConfigFunc(serverConfig) {
	// if (serverConfig instanceof Object) return new Error('write config error!')
	for (let key in serverConfig) {
		uni.setStorageSync(key, serverConfig[key])
	}
}

export {
	writeConfigFunc
}