import Vue from 'vue'
import Vuex from 'vuex'

// // 模块
// import user from './model/user.js'

Vue.use(Vuex)
const HASLOGIN = uni.getStorageSync('hasLogin') || false
const USEINFO = uni.getStorageSync('userInfo') || {}
const SYSID = uni.getStorageSync('sysId') || ''
const ORGID = uni.getStorageSync('orgId') || ''
const DEPARTMENTID = uni.getStorageSync('departmentId') || ''
const CONTRACTORID = uni.getStorageSync('contractorId') || ''
const IMGSERVER = uni.getStorageSync('imgServer') || {}

const store = new Vuex.Store({
	state:{
		hasLogin: HASLOGIN,
		userInfo: USEINFO,
		sysId: SYSID,
		orgId: ORGID,
		departmentId: DEPARTMENTID,
		contractorId: CONTRACTORID,
		imgServer: IMGSERVER
	},
	getters: {
		hasLogin: state => state.hasLogin,
		userInfo: state => state.userInfo,
		sysId: state => state.sysId,
		orgId: state => state.orgId,
		departmentId: state => state.departmentId,
		contractorId: state => state.contractorId,
		imgServer: state => state.imgServer
	},
	mutations: {
		login(state, provider) {
			state.hasLogin = true,
			state.userInfo = provider
			uni.setStorageSync('userInfo', provider)
		},
		logout(state) {
			state.hasLogin = false
			uni.setStorageSync('hasLogin', false)
			state.userInfo = {}
			uni.setStorageSync('userInfo', {})
			uni.removeStorageSync('userInfo')
		},
		setHasLogin(state, provider) {
			state.hasLogin = provider
			uni.setStorageSync('hasLogin', state.hasLogin)
		},
		setUserInfo(state, provider) {
			state.userInfo = provider
			uni.setStorageSync('userInfo', state.userInfo)
		},
		setSysId(state, provider) {
			state.sysId = provider
			uni.setStorageSync('sysId', state.sysId)
		},
		setOrgId(state, provider) {
			state.orgId = provider
			uni.setStorageSync('orgId', state.orgId)
		},
		setContractorId(state, contractorId) {
			state.contractorId = contractorId
			uni.setStorageSync('contractorId', state.contractorId)
		},
		setDepartmentId(state, departmentId) {
			state.departmentId = departmentId
			uni.setStorageSync('departmentId', state.departmentId)
		},
		setImgServer(state, imgServer) {
			state.imgServer = imgServer
			uni.setStorageSync('imgServer', state.imgServer)
		}
	},
	actions: {
		logout({commit}) {
			commit('logout')
		},
		setHasLogin({commit}, hasLogin) {
			commit('setHasLogin', hasLogin)
		},
		setUserInfo({commit}, info) {
			commit('setUserInfo', info)
		},
		setSysId({commit}, sysId) {
			commit('setSysId', sysId)
		},
		setOrgId({commit}, orgId) {
			commit('setOrgId', orgId)
		},
		setContractorId({commit}, contractorId) {
			commit('setContractorId', contractorId)
		},
		setImgServer({commit}, imgServer) {
			commit('setImgServer', imgServer)
		}
	}
})
export default store