
const user = {
	state: {
		userInfo: {},
		userId: '',
		accessTokenKey: '',
		accessToken: ''
	},
	getters: {
		getUserId(state) {
			return state.userId
		},
		getAccessTokenKey(state) {
			return state.accessTokenKey
		},
		getAccessToken(state) {
			return state.accessToken
		}
	}
}

export default user