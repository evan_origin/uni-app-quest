
import getHead from './getHead.js'
import getUrlPrefix from './gerUrlPrefix.js'
import serverConfig from '../static/serverConfig.js'
// import * as $utils from '../utils/index.js'
/**
 * @description http请求的post方法
 * @param {Object} type 请求头类型 (ygst, inner等)
 * @param {Object} url 接口地址
 * @param {Object} data 请求携带的参数
 * @param {Object} sec 是否加密
 */
async function post(type, url, data, sec) {
	let head = await getHead(type, url, data)

	let urlPrefix = await getUrlPrefix(type, url)
	// 如果是企盟家的接口需要入参需要加密
	data = (['swagger', 'ygst', 'hardware'].indexOf(type) < 0) ? encodeURIComponent(JSON.stringify(data)) : data
	return new Promise((resolve, reject) => {
		uni.request({
			url: urlPrefix,
			header: head,
			method: 'POST',
			data: data,
			success(response) {
				// 由于接口设计问题，部分错误会返回一个 ERR_开头的异常信息字符串,如：ERR_{code:'',msg:''}
				if (/ERR_/.test(response.data)) {
					let errorJson = JSON.parse(response.data.replace('ERR_{', '{'))
					reject(errorJson)
				}
				if (['swagger', 'login', 'gis', 'ygst', 'hardware'].indexOf(type) < 0) {
					response.data = decodeURIComponent(response.data)
					resolve(response.data)
				}
				resolve(response.data)
			},
			fail(err) {
				console.log('在里面的 err', err)
				reject(err)
			}
		})
	})
}
async function get() {}

export {
	get,
	post
}