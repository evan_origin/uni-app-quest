
import serverConfig from '../static/serverConfig.js'

async function getUrlPrefix(type, url) {
	return new Promise((resolve, reject) => {
		let urlPrefix = ''
		switch (type) {
		    case 'outer':
		      urlPrefix = serverConfig.outerServerHost + serverConfig.outerServerPort
			  resolve(urlPrefix)
		      break
		    case 'workflow':
		      urlPrefix = serverConfig.workflowServerHost + serverConfig.workflowServerPort
			  resolve(urlPrefix)
		      break
		    case 'inner':
		      urlPrefix = serverConfig.innerServerHost + serverConfig.innerServerPort
			  resolve(urlPrefix)
		      break
		    case 'oauth':
		      urlPrefix = serverConfig.oauthServerHost + serverConfig.oauthServerPort
			  resolve(urlPrefix)
		      break
		    case 'ygst':
		      urlPrefix = serverConfig.ygstServerHost + serverConfig.ygstServerPort + '/' + url
			  resolve(urlPrefix)
		      break
		    case 'hardware': // 和安卓硬件交互
		      urlPrefix = serverConfig.hardwareServerHost + serverConfig.hardwareServerPort + '/' + url
			  resolve(urlPrefix)
		      break
		    case 'swagger':
		      urlPrefix = serverConfig.swaggerServerHost + serverConfig.swaggerServerPort + '/' + url
			  resolve(urlPrefix)
		      break
		    default:
		      urlPrefix = serverConfig.swaggerServerHost + serverConfig.swaggerServerPort + '/' + url
			  resolve(urlPrefix)
		  }
	})
}
export default getUrlPrefix