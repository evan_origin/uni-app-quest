/***
 * 文件服务：文件的上传与下载
 */
// import axios from 'axios'
import CryptoJS from './crypto-js'
// import post from './post'
import getUrlPrefix from './gerUrlPrefix.js'
/**
 * 转换字节顺序为32一组
 * @param val
 * @returns {number}
 */
function swapX32 (val) {
  return (((val & 0xFF) << 24) | ((val & 0xFF00) << 8) | ((val >> 8) & 0xFF00) | ((val >> 24) & 0xFF)) >>> 0
}

/**
 * 将ArrayBuffer 转换为 CryptoJS 的 WordArray
 * @param arrayBuffer Buffer
 * @returns wordArray
 */
function arrayBufferToWordArray (arrayBuffer) {
  var fullWords = Math.floor(arrayBuffer.byteLength / 4)
  var bytesLeft = arrayBuffer.byteLength % 4

  var u32 = new Uint32Array(arrayBuffer, 0, fullWords)
  var u8 = new Uint8Array(arrayBuffer)

  var cp = []
  for (var i = 0; i < fullWords; ++i) {
    cp.push(swapX32(u32[i]))
  }

  if (bytesLeft) {
    var pad = 0
    for (let i = bytesLeft; i > 0; --i) {
      pad = pad << 8
      pad += u8[u8.byteLength - i]
    }

    for (let j = 0; j < 4 - bytesLeft; ++j) {
      pad = pad << 8
    }

    cp.push(pad)
  }

  return CryptoJS.lib.WordArray.create(cp, arrayBuffer.byteLength)
}

/**
 * 获取文件的hash值
 * @param file 文件数据
 * @return {Promise}
 */
function readFileHash (file) {
  return new Promise((resolve, reject) => {
    // var reader = new window.FileReader()
	let reader = new plus.io.FileReader()
    reader.onloadend = e => {
      let hash = CryptoJS.MD5(arrayBufferToWordArray(e.target.result)).toString()
      resolve(hash)
    }
    reader.onerror = e => {
      console.error('在获取文件hash时，读取文件失败')
      reject(e)
    }
    reader.readAsArrayBuffer(file)
  })
}

/**
 * 上传文件
 * @param fileName 文件名
 * @param file 文件对象
 * @param targetId 文件归属组织(选填)
 * @returns {*}
 */
async function upload ({fileName, file, targetId}) {
  let hash = await readFileHash(file)
  let fileInfo = {'fileName': fileName, 'fileHash': hash}
  let getUrlPrefix = await getUrlPrefix('inner', 'ehome.org.file.upload.url.get')
  return new Promise((resolve, reject) => {
	uni.request({
		url: getUrlPrefix,
		method: 'POST',
		data: {},
		success(reqponse) {
			let formData = new window.FormData()
			if (targetId) {
			  fileInfo.targetId = targetId
			}
			formData.append('file', file)
			formData.append('ct', JSON.stringify(fileInfo))
			uni.request({
				url: response.url,
				method: 'POST',
				data: formData,
				success(response) {
					if (/ERR_/.test(response.data)) {
					  let errorJson = JSON.parse(response.data.substr(4))
					  reject(errorJson)
					} else {
					  resolve(response.data)
					}
				}
			})
		},
		fail(e) {
			reject(e)
		}
	})
    // post('inner', 'ehome.org.file.upload.url.get', {}).then(response => {
    //   // console.log('fileUploadUrlGet:', response)
    //   // let formData = new window.FormData()
    //   // if (targetId) {
    //     // fileInfo.targetId = targetId
    //   }
    //   // formData.append('file', file)
    //   // formData.append('ct', JSON.stringify(fileInfo))
    //   // console.log('url', response)
    //   // axios({
    //   //   method: 'post',
    //   //   url: response.url,
    //   //   responseType: 'json',
    //   //   data: formData
    //   // }).then(response => {
    //   //   // console.log('fileUploadUrlGetUploadOk:', response)
    //   //   if (/ERR_/.test(response.data)) {
    //   //     let errorJson = JSON.parse(response.data.substr(4))
    //   //     reject(errorJson)
    //   //   } else {
    //   //     resolve(response.data)
    //   //   }
    //   // }).catch(e => {
    //   //   reject(e)
    //   // })
    // })
  })
}

/***
 * 下载文件
 */
async function download(url) {
  let promise = new Promise((resolve, reject) => {
    // axios({
    //   method: 'get',
    //   url: url,
    //   responseType: 'blob'
    // }).then(response => {
    //   if (/ERR_/.test(response.data)) {
    //     let errorJson = JSON.parse(response.data.substr(4))
    //     reject(errorJson)
    //   } else {
    //     let reader = new window.FileReader()
    //     reader.onloadend = function (e) {
    //       resolve(e.target.result)
    //     }
    //     reader.readAsDataURL(response.data)
    //   }
    // }).catch(e => {
    //   console.error('下载文件请求失败', e)
    //   reject(e)
    // })
  })
  return promise
}

export {upload, download}
