
import serverConfig from '../static/serverConfig.js'
import store from '../store/index.js'

import md5 from './crypto-js/hmac-md5.js'

/**
 * @description 获取签名 （签名md5加密）
 * @param {Object} ak accessTokenKey 
 * @param {Object} at accessToken
 * @param {Object} ct 参数
 * @param {Object} sec 是否加密
 * @param {Object} url 请求地址
 */
async function getSign(ak, at, ct, sec, url) {
	let clientId = serverConfig.clientId
	let pa = at + (sec ? [clientId, ak, ct, sec, url] : [clientId, ak, ct])
	    .sort().join('').replace(/\t|\r|\n/g, '') + at
	let sign
	try {
		sign = md5(pa,at).toString().toUpperCase()
	} catch (e) {
		console.log('签名错误')
		console.log(e)
	}
	return sign
}


async function getHead(type, url, data) {
	let clientId = serverConfig.clientId
	let args = []
	let uid = store.state.userInfo.userId || uni.getStorageSync('userInfo').userId
	let ak = store.state.userInfo.accessTokenKey || uni.getStorageSync('userInfo').accessTokenKey
	let at = store.state.userInfo.accessToken || uni.getStorageSync('userInfo').accessToken
	let ct = JSON.stringify(data)
	let sec = 'F'
	let sign
	switch (type) {
		case 'oauth':
		  args.push(clientId)
		  args.push(url)
		  args.push('')
		  break
		case 'inner':
		  args.push(uid)
		  args.push(url)
		  args.push('')
		  break
		case 'outer':
		case 'workflow':
		case 'outerOauth':
		  sign = await getSign(ak, at, ct, sec, url)
		  args.push(clientId)
		  args.push(ak)
		  args.push(sign)
		  args.push(sec)
		  args.push(url)
		  args.push('')
		  break
		case 'swagger':
		  sign = await getSign(ak, at, ct)
		  args.push(clientId)
		  args.push(ak)
		  args.push(sign)
		  args.push('')
		  let header = {
			'Accept': '*/*',
			'skynet-head-params': args.join(';')
		  }
		  return header
		case 'ygst':
		  sign = await getSign(ak, at, ct)
		  args.push(clientId)
		  args.push(ak)
		  args.push(sign)
		  args.push('')
		  let head = {
		    'Accept': '*/*',
		    'ygst-head-params': args.join(';')
		  }
		  return head
		case 'hardware':
		  sign = await getSign(ak, at, ct)
		  args.push(clientId)
		  args.push(ak)
		  args.push(sign)
		  args.push('')
		  let hardwareHead = {
		          'Accept': '*/*',
		          'ygst-head-params': args.join(';')
		  }
		return hardwareHead
	}
	  let header = {
	    'Content-Type': 'application/octet-stream',
	    'ehome-head-params': args.join(';')
	  }
	console.log('getHead.js ------ header:::', header)
	return header
}
export default getHead