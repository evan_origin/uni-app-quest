
const serverConfig = {
	  'clientId': '2E7966B47EA04112BBC03F3B9992E6F7',
	  'outerServerHost': 'http://172.30.1.170', // 'http://cpf.7mhome.com:9000'
	  'outerServerPort': ':9000',
	  'workflowServerHost': 'http://172.30.1.170', // 'http://wflow.7mhome.com:9100'
	  'workflowServerPort': ':9100',
	  'innerServerHost': 'http://172.30.1.170', // 'http://cpf.7mhome.com:9300'
	  'innerServerPort': ':9300',
	  'oauthServerHost': 'http://172.30.1.170', // 'http://oauth.7mhome.com:9200'
	  'oauthServerPort': ':9200',
	  'swaggerServerHost': 'http://172.30.1.170', // 'http://175.42.30.41:10819'
	  'swaggerServerPort': ':6200',
	  'superMapServerHost': 'http://172.30.1.170', // 'http://175.42.30.41:9200'
	  'superMapServerPort': ':8091',
	  'ygstServerHost': 'http://172.30.1.170', // 'http://175.42.30.41:9200'
	  'ygstServerPort': ':6300',
	  'hardwareServerHost': 'http://172.30.1.170', // 'http://175.42.30.41:9200'
	  'hardwareServerPort': ':6400',
	  'imgServer': {
		  'host' : 'http://175.42.30.41',
		  'port': '8001'
	  },
	  'imgServerHost': 'http://175.42.30.41', // 'http://img.7mhome.com:8001'
	  'imgServerPort': '8001',
	  'webSocketURL': 'ws://172.30.0.240:8888/websocket/websocket',
	  'PcTest': false,
	  'requestType': 'tomcat',
	  'vconsole': false,
	  'tomcatPath': '/ehome-org/',
	  'orgServer': 'http://172.30.1.160:92/org/#/' // 组织中心地址 外网 http://org.7mhome.com/#/
}
export default serverConfig