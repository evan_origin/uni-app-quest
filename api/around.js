import * as http from '../http/index.js'

export default {
	/**
	 * @description: 周边商家分页列表信息
	 * @author: LinW
	 * @createDate: 2019-09-09 15:16
	 * @param: currentPage  当前页
	 * @param: numPerPage 分页大小
	 * @param: orgId  组织机构
	 * @param: peripheralName  组织机构
	 * @param: type 商家类别
	 * @return:
	 */
	ygstPeripheralFoodShoppageGet({currentPage, numPerPage, orgId, peripheralName, type}) {
		  let params = {}
		  if (currentPage !== 'undefined') {
		    params['currentPage'] = currentPage
		  }
		  if (numPerPage !== 'undefined') {
		    params['numPerPage'] = numPerPage
		  }
		  if (orgId !== 'orgId') {
		    params['orgId'] = orgId
		  }
		  if (peripheralName !== 'peripheralName') {
		    params['peripheralName'] = peripheralName
		  }
		  if (type !== 'type') {
		    params['type'] = type
		  }
		  return http.post('ygst', 'ygst/peripheral/food/shop/page/get', params)
	},
	/**
	 * @description: 根据主键获取周边商家信息
	 * @author: LinW
	 * @createDate: 2019-09-10 10:03
	 * @param: peripheralId 主键id
	 * @return:
	 */
	ygstPeripheralFoodShopGet(peripheralId) {
		let params = {
			'peripheralId': peripheralId
		}
		return http.post('ygst', 'ygst/peripheral/food/shop/get', params)
	},
	/**
	 * @description: 获取巡查记录列表
	 * @author: LinW
	 * @createDate: 2019-09-10 12:29
	 * @param: beginTime 巡查开始时间
	 * @param: endTime 巡查结束时间
	 * @param: orgId 组织ID
	 * @param: page 当前显示页
	 * @param: pageSize 每页显示条数
	 * @param: peripheralId  商家主键id
	 * @param: peripheralName  商家名称
	 * @return:
	 */
	ygstPatrolRecordListGet({beginTime, endTime, orgId, page, pageSize, peripheralId, peripheralName}) {
		  let params = {}
		  if (beginTime !== 'undefined') {
		    params['beginTime'] = beginTime
		  }
		  if (endTime !== 'undefined') {
		    params['endTime'] = endTime
		  }
		  if (orgId !== 'undefined') {
		    params['orgId'] = orgId
		  }
		  if (page !== 'undefined') {
		    params['page'] = page
		  }
		  if (pageSize !== 'undefined') {
		    params['pageSize'] = pageSize
		  }
		  if (peripheralId !== 'undefined') {
		    params['peripheralId'] = peripheralId
		  }
		  if (peripheralName !== 'undefined') {
		    params['peripheralName'] = peripheralName
		  }
		  return http.post('ygst', 'ygst/patrol/record/list/get', params)
	}
}