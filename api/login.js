
import * as http from '../http/index.js'

export default {
	ehomeLogin({loginName, pwd}) {
		let params = {loginName, pwd}
		return http.post('oauth', 'ehome.ac.login.get', params)
	},
	getSelfOsysListPost() {
		return http.post('outer', 'ehome.osys.self.list.get', {})
	}
}