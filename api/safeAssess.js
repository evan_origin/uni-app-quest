/* 安全评估页面 */
import * as http from '../http/index.js'

export default {
	ygstAppExceptionContractorInfoGet({
		departmentId
	}) {
		let params = {
			departmentId
		}
		return http.post('hardware', 'ygst/app/exception/contractor/info/get', params)
	},
	/**
	 * 获取食堂列表(ID和名称) 获取食堂列表
	 */
	ygstConstraManagementDeptListGet({
		orgId
	}) {
		let params = {
			orgId
		}
		return http.post('ygst', 'ygst/dept/base/list/get', params)
	},
	/**
	 * @description 获取食品经营许可证信息
	 * @param orgId 承包商ID（组织ID）
	 */
	getLicenseInfo({
		orgId
	}) {
		let params = {
			orgId
		}
		return http.post('ygst', '/ygst/pc/license/info/get', params)
	},
	/**
	 * @description 获取食品经营许可证信息
	 * @param orgId 承包商ID（组织ID）
	 */
	getLicenseInfo({
		orgId
	}) {
		let params = {
			orgId
		}
		return http.post('ygst', '/ygst/pc/license/info/get', params)
	},
	/**
	 * @description: 获取营业执照信息
	 * @author: LinW (evan_origin@163.com)
	 * @createDate: 2019-12-30 17:05
	 * @param: orgId 组织ID
	 * @return:
	 */
	ygstPcBusinessLicenseDetailsGet({
		orgId
	}) {
		let params = {
			orgId
		}
		return http.post('ygst', '/ygst/pc/businesslicense/detail/get', params)
	},

	/**
	 * @description: 根据食堂编号、承包商ID获取在岗人员信息
	 * @author: linwen (linwen@fjxhx.com)
	 * @createDate: 2020-01-14 14:25
	 * @param: contractorId 承包商id
	 * @param: departmentId 食堂编号
	 * @return:
	 */
	ygstEmployeeOnpostDeptcontraListGet({
		contractorId,
		departmentId
	}) {
		let params = {
			contractorId,
			departmentId
		}
		return http.post('ygst', '/ygst/employee/onpost/deptcontra/list/get', params)
	},
	/**
	 * @description 获取在岗人员的个人信息
	 * @param departmentId 食堂ID
	 * @param userId 人员ID
	 */
	getOnPostPersonalInfo({
		departmentId,
		userId,
		contractorId
	}) {
		let params = {
			departmentId,
			userId,
			contractorId
		}
		return http.post('ygst', '/ygst/employee/onpost/info/get', params)
	},
	/**
	 * @description: 根据食堂ID查询日巡查清单
	 * @author: LinW (evan_origin@163.com)
	 * @createDate: 2019-12-30 14:51
	 * @param: orgId 组织Id
	 * @param: departmentId 食堂ID
	 */
	ygstInspectMenuDeptGet({
		orgId,
		departmentId
	}) {
		let params = {
			orgId,
			departmentId
		}
		return http.post('ygst', '/ygst/inspectmenu/dept/get', params)
	},
	/**
	 * @description 获取责任险信息
	 * @param departmentId
	 * @param orgId 承包商ID（组织ID）
	 */
	getInsuranceInfo({
		orgId
	}) {
		let params = {
			orgId
		}
		return http.post('ygst', '/ygst/pc/insurance/list/get', params)
	},
	/**
	 * @description 获取责任险信息
	 * @param departmentId
	 * @param orgId 承包商ID（组织ID）
	 */
	getInsuranceInfo({
		orgId
	}) {
		let params = {
			orgId
		}
		return http.post('ygst', '/ygst/pc/insurance/list/get', params)
	},
	/**
	 * @description: 日巡查清单各项目列表查询
	 * @author: LinW (evan_origin@163.com)
	 * @createDate: 2019-12-30 10:14
	 * @param: inspectMenuId 日巡查清单主键ID
	 * @return: <Promise>
	 */
	ygstInspectMenuItemListGet(inspectMenuId) {
		let params = {
			inspectMenuId
		}
		return http.post('ygst', '/ygst/inspectmenu/item/list/get', params)
	},
	/**
	 * @description: 更新日巡查清单项目
	 * @author: LinW (evan_origin@163.com)
	 * @createDate: 2019-12-30 10:15
	 * @param: dayInspectFlag 检查标识（0-未检 1-已检） ,
	 * @param: fileId 文件ID列表 ,
	 * @param: inspectItemId 主键
	 * @param: remark 备注
	 * @return: <Promise>
	 */
	ygstInspectMenuItemUpdate(arg) {
		let params = { ...arg
		}
		return http.post('ygst', '/ygst/inspectmenu/item/update', params)
	},
	/**
	 * @description: 日巡查清单列表查询
	 * @author: LinW (evan_origin@163.com)
	 * @createDate: 2019-11-16 11:06
	 * @param: beginInspectMenuDate (options) 记录时间-开始段
	 * @param: endInspectMenuDate (options) 记录时间-结束段
	 * @param: departmentId 食堂ID
	 * @param: page 当前显示页
	 * @param: pageSize  每页显示条数
	 * @return: <Promise>
	 */
	ygstInspectMenuListGet({
		departmentId,
		orgId,
		page,
		pageSize
	}) {
		let params = {
			departmentId,
			orgId,
			page,
			pageSize
		}
		return http.post('ygst', '/ygst/inspectmenu/list/get', params)
	},

	/**
	 * @description: app安全综合评估分页获取异常列
	 * @author: LinW (evan_origin@163.com)
	 * @createDate: 2019-12-28 10:48
	 * @param: anomalousType (string) 异常类型（01证件 02设备 03人员 04食品）
	 * @param: contractorId (string, optional) 承包商id
	 * @param: departmentId (string) 食堂id
	 * @param: page (integer) 当前显示页，取值范围[1,10000000]
	 * @param: pageSize (integer) 每页显示条数，取值范围[1,100]
	 * @param: today (string, optional) 时间（今日异常传入时间，历史异常无需）
	 * @return:
	 */
	ygstAppExceptionContractorListGet({
		departmentId,
		anomalousType,
		page,
		pageSize,
		contractorId,
		today
	}) {
		let params = {
			departmentId,
			anomalousType,
			page,
			pageSize
		}
		// 非必传
		if (typeof contractorId !== 'undefined') {
			params['contractorId'] = contractorId
		}
		if (typeof today !== 'undefined') {
			params['today'] = today
		}
		return http.post('hardware', 'ygst/app/exception/contractor/list/get', params)
	}
}
