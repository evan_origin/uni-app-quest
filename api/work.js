/**/

import * as http from '../http/index.js'

export default {
	/**
	 * 获取今日所有食堂异常总数列表
	 * @param orgId {String} orgId  
	 */
	getTodayExceptionCountList({orgId}) {
		let params = {orgId}
		return http.post('hardware', 'ygst/exception/today/alldept/info/get', params)
	},
	/**
	 * 获取今日各食堂检查记录
	 */
	getTodayCheckCount({departmentId}) {
		let params = {departmentId}
		return http.post('ygst', 'ygst/inspectmenu/statistics/dept/get', params)
	},
	/**
	 * 获取今日异所有食堂异常总数
	 */
	getTodayExceptionCount({orgId}) {
		let params = {orgId}
		return http.post('hardware', 'ygst/exception/today/handle/info/get', params)
	},
	/**
	 * 获取周边商家数目
	 */
	getShopCount({orgId}) {
		let params = {orgId}
		return http.post('ygst', 'ygst/peripheral/food/shop/type/count/get', params)
	},
	/**
	 * 获取异常信息
	*/
	ygstAppExceptionDetailInfoGet(exceptionId) {
		let params = {exceptionId}
		return http.post('hardware', 'ygst/app/exception/detail/info/get', params)
	},
	/**
	 * @description: 获取异常处置信息
	 * @author: LinW
	 * @createDate: 2019-08-13 17:21
	 * @param: mod
	 * @return:
	 */
	ygstExceptionInfoGet(exceptionId) {
		let params = {exceptionId}
		return http.post('hardware', 'ygst/exception/explanation/info/get', params)
	}
}