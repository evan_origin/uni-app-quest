import * as http from '../http/index.js'

export default {
	ygstCameraRegionNumberList({departmentId}) {
		let params = {departmentId}
		return http.post('hardware', 'ygst/camera/region/number/list', params)
	}
}