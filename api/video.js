import * as http from '../http/index.js'

export default {
	ygstCameraRegionListGet({departmentId, regionId, page, pageSize})  {
		let params = {departmentId, regionId, page, pageSize}
		return http.post('hardware', 'ygst/camera/region/list/get', params)
	}
}