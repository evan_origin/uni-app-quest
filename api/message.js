import * as http from '../http/index.js'

export default {
	/**
	 * @description 获取今日异常
	 * @param {departmentId}  
	 */
	ygstAppExceptionContractorInfoGet ({departmentId}) {
	  let params = {departmentId}
	  return http.post('hardware', 'ygst/app/exception/contractor/info/get', params)
	},
	/**
	 * @description 获取今日陪餐领导
	 * @param {Object} departmentId
	 */
	getTodayLeadInfo(departmentId) {
		return http.post('ygst', 'ygst/accompany/schedule/today/leadinfo', {'departmentId': departmentId})
	},
	/**
	 * @description: 食堂规章查询
	 * @author: LinW (evan_origin@163.com)
	 * @createDate: 2019-12-30 16:49
	 * @param: orgId 承包商ID（组织ID）
	 */
	ygstRegulationGet({orgId}) {
		let params = {orgId}
		return http.post('ygst', 'ygst/regulation/get', params)
	},
	/**
	 * @description: 查看食堂公示列表
	 * @author: LinW
	 * @createDate: 2019-08-12 15:32
	 * @param: departmentId  食堂ID
	 * @param: orgId 承包商ID（组织ID）
	 * @param: page 当前显示页
	 * @param: pageSize 每页显示条数
	 * @return:
	 */
	ygstCanteenListGet({departmentId, orgId, page, pageSize}) {
		let params = {departmentId, orgId, page, pageSize}
		return http.post('ygst', 'ygst/canteen/APP/list/get', params)
	},
		/**
	 * @description: 查看食堂公示详情
	 * @author: LinW
	 * @createDate: 2019-08-13 9:38
	 * @param: canteenPublicityId 主键ID
	 * @return:
	 */
		ygstCanteenGet({ canteenPublicityId }) {
			let params = { canteenPublicityId }
			return http.post('ygst', '/ygst/canteen/get', params)
		}
}