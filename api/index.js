import * as http from '../http/index.js'
import login from './login.js'
import work from './work.js'
import profile from './profile.js'
import video from './video.js'
import safeAssess from './safeAssess.js'
import around from './around.js'
import message from './message.js'
const api = {
	/**
	 * 指定体系下获取当前用户所在的组织列表
	 * @param {Object} sysId
	 */
	osysOrgSelfListGet(sysId) {
		return http.post('outer', 'ehome.osys.org.self.list.get', {'sysId': sysId})
	},
	/**
	 * @description: 根据食堂ID，获取供应商列表
	 * @author: LinW (evan_origin@163.com)
	 * @createDate: 2020-01-02 10:23
	 * @param: departmentId 食堂ID
	 * @return:
	 */
	ygstConstramanageContraListGet({departmentId}) {
		let params = {departmentId}
		return http.post('ygst', 'ygst/constramanage/contra/list/get', params)
	},
	/**
	 * @description: 管理方登录，获取体系-组织信息
	 * 
	 */
	ygstConstraManageManagerLoginListGet() {
		return http.post('ygst', 'ygst/constramanage/managerlogin/list/get', {})
	}
}


Object.assign(api, login, work, profile, video, safeAssess, around, message)

export default api