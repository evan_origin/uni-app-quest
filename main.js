import Vue from 'vue'
import App from './App'

// 挂载全局的请求
import * as http from './http/index.js'
Vue.prototype.$http = http

// api
import Request from './api/index.js'
Vue.prototype.$request = Request

// 写入全局配置
import * as config from './config/config.js'
import serverConfig from  './static/serverConfig.js' // '../static/serverConfig.js'
config.writeConfigFunc(serverConfig)

// 全局挂载工具函数
import * as utils from './utils/index.js'
Vue.prototype.$utils = utils

// 挂载全局状态管理
import store from './store'
Vue.prototype.$store = store

// 引入组件/插件，全局使用
import uniIcons from '@/components/uni-icons/uni-icons.vue'
import uniNavBar from '@/components/uni-nav-bar/uni-nav-bar.vue'
import uniSection from '@/components/uni-section/uni-section.vue'
import uniBadge from '@/components/uni-badge/uni-badge.vue'
import uniLoadMore from '@/components/uni-load-more/uni-load-more.vue'
import countUp from '@/components/p-countUp/countUp.vue'
import uniTransition from '@/components/uni-transition/uni-transition.vue'
import navBar from '@/components/navBar/navBar.vue'
import InspectionList from '@/components/InspectionList/InspectionList.vue'


Vue.component('uni-icons', uniIcons)
Vue.component('uni-nav-bar', uniNavBar)
Vue.component('uni-section', uniSection)
Vue.component('uni-badge', uniBadge)
Vue.component('uni-load-more', uniLoadMore)
Vue.component('countup', countUp)
Vue.component('uni-transition', uniTransition)
Vue.component('navBar', navBar)
Vue.component('InspectionList', InspectionList)

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
