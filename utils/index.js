/**
 * @description 判断对象是否为空对象
 * @param {Object} e  判断的对象
 * @return {boolean} true || false
 */
function isEmptyObject(e) {
	// 如果部署对象直接return
	if(!e) return true
	// vue框架给数组添加了一个$remove的方法，所以对数组单独判断
	if (e instanceof Array) {
		if (e.length > 0) {
			return false
		} else {
			return true
		}
	}
	for (var t in e) {
		return false
	}
	 return true
}
/**
 * @param date 时间搓
 * @param fmt  需要转换的时间格式：例如： 'yyyy-MM-dd hh:mm:ss'
 * @example formatDate(+new Date,'yy-MM-dd hh:mm')
 */
function formatDate (date, fmt) {
  // 年份
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  // 月份/日/时/分/秒 正则对象表
  let o = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'h+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds()
  }
  // 使用 for 去遍历 构造出来的 正则函数，并且对 时间搓 date进行匹配
  for (let k in o) {
    if (new RegExp(`(${k})`).test(fmt)) {
      let str = o[k] + '' // 转换成字符串
      // 如果匹配到一位数，就直接输出，或者补个0
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : padLeftZero(str))
    }
  }
  return fmt
}
// date 补0   辅助函数
function padLeftZero (str) {
  return ('00' + str).substr(str.length)
}
export {
	isEmptyObject,
	formatDate
}
